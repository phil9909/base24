package base24_test

import (
	"encoding/hex"
	"fmt"
	"strings"
	"testing"

	. "github.com/onsi/gomega"
	"gitlab.com/phil9909/base24"
)

func TestSimpleInputOutput(t *testing.T) {

	var tests = []struct {
		hex string
		b24 string
	}{
		{"", ""},
		{"00000000", "ZZZZZZZ"},
		{"000000000000000000000000", "ZZZZZZZZZZZZZZZZZZZZZ"},
		{"00000001", "ZZZZZZA"},
		{"000000010000000100000001", "ZZZZZZAZZZZZZAZZZZZZA"},
		{"00000010", "ZZZZZZP"},
		{"00000030", "ZZZZZCZ"},
		{"88553311", "5YEATXA"},
		{"FFFFFFFF", "X5GGBH7"},
		{"FFFFFFFFFFFFFFFFFFFFFFFF", "X5GGBH7X5GGBH7X5GGBH7"},
		{"FFFFFFFFFFFFFFFFFFFFFFFF", "x5ggbh7x5ggbh7x5ggbh7"},
		{"1234567887654321", "A64KHWZ5WEPAGG"},
		{"1234567887654321", "a64khwz5wepagg"},
		{"FF0001FF001101FF01023399", "XGES63FZZ247C7ZC2ZA6G"},
		{"FF0001FF001101FF01023399", "xges63fzz247c7zc2za6g"},
		{"25896984125478546598563251452658", "2FC28KTA66WRST4XAHRRCF237S8Z"},
		{"25896984125478546598563251452658", "2fc28kta66wrst4xahrrcf237s8z"},
		{"00000001", "ZZZZZZA"},
		{"00000002", "ZZZZZZC"},
		{"00000004", "ZZZZZZB"},
		{"00000008", "ZZZZZZ4"},
		{"00000010", "ZZZZZZP"},
		{"00000020", "ZZZZZA4"},
		{"00000040", "ZZZZZCP"},
		{"00000080", "ZZZZZ34"},
		{"00000100", "ZZZZZHP"},
		{"00000200", "ZZZZZW4"},
		{"00000400", "ZZZZARP"},
		{"00000800", "ZZZZ2K4"},
		{"00001000", "ZZZZFCP"},
		{"00002000", "ZZZZ634"},
		{"00004000", "ZZZABHP"},
		{"00008000", "ZZZC4W4"},
		{"00010000", "ZZZB8RP"},
		{"00020000", "ZZZG5K4"},
		{"00040000", "ZZZRYCP"},
		{"00080000", "ZZAKX34"},
		{"00100000", "ZZ229HP"},
		{"00200000", "ZZEFPW4"},
		{"00400000", "ZZT7GRP"},
		{"00800000", "ZAAESK4"},
		{"01000000", "ZCCK7CP"},
		{"02000000", "ZB32E34"},
		{"04000000", "Z4HETHP"},
		{"08000000", "ZP9KZW4"},
		{"10000000", "AG8CARP"},
		{"20000000", "CSHB2K4"},
		{"40000000", "3694FCP"},
		{"80000000", "53PP634"},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("%s --encode-> %s", test.hex, test.b24), func(t *testing.T) {
			g := NewGomegaWithT(t)

			in, err := hex.DecodeString(test.hex)
			g.Expect(err).NotTo(HaveOccurred())

			out, err := base24.StdEncoding.EncodeToString(in)
			g.Expect(err).NotTo(HaveOccurred())

			upper24 := strings.ToUpper(test.b24)
			g.Expect(out).To(Equal(upper24))
		})

		t.Run(fmt.Sprintf("%s --decode-> %s", test.b24, test.hex), func(t *testing.T) {
			g := NewGomegaWithT(t)

			expected, err := hex.DecodeString(test.hex)
			g.Expect(err).NotTo(HaveOccurred())

			out, err := base24.StdEncoding.DecodeString(test.b24)
			g.Expect(err).NotTo(HaveOccurred())

			g.Expect(out).To(Equal(expected))
		})
	}
}

func TestInvalidEncodeInput_NotMultipleOf4(t *testing.T) {
	g := NewGomegaWithT(t)

	in, err := hex.DecodeString("0000000000")
	g.Expect(err).NotTo(HaveOccurred())

	_, err = base24.StdEncoding.EncodeToString(in)
	g.Expect(err).To(MatchError(ContainSubstring("multiple of 4")))
}

func TestInvalidDecodeInput_NotMultipleOf7(t *testing.T) {
	g := NewGomegaWithT(t)

	_, err := base24.StdEncoding.DecodeString("ZZZZZZZZ")
	g.Expect(err).To(MatchError(ContainSubstring("multiple of 7")))
}

func TestInvalidDecodeInput_IllegalChar(t *testing.T) {
	g := NewGomegaWithT(t)

	_, err := base24.StdEncoding.DecodeString("ZZZZZZO")
	g.Expect(err).To(MatchError(ContainSubstring(`'O'`)))
}

func TestInvalidEncodingAlphabet(t *testing.T) {
	var tests = []struct {
		alphabet string
		panic    string
	}{
		{"to short", "encoding alphabet is not 24-bytes long"},
		{"ZAC2B3EF4GH5TK67P8RS9WX\r", "encoding alphabet contains newline character"},
		{"ZAC2B3EF4GH5TK67P8RS9WX\n", "encoding alphabet contains newline character"},
	}

	for _, test := range tests {
		t.Run(fmt.Sprintf("invalid alphabet %q", test), func(t *testing.T) {
			g := NewGomegaWithT(t)
			g.Expect(func() {
				base24.NewEncoding(test.alphabet)
			}).To(PanicWith(test.panic))
		})
	}
}

func FuzzRoundTrip(f *testing.F) {
	f.Fuzz(func(t *testing.T, data []byte) {
		g := NewGomegaWithT(t)

		encoded, err := base24.StdEncoding.EncodeToString(data)
		if len(data)%4 != 0 {
			g.Expect(err).To(MatchError(ContainSubstring("multiple of 4")))
		} else {
			g.Expect(err).NotTo(HaveOccurred())

			decoded, err := base24.StdEncoding.DecodeString(encoded)
			g.Expect(err).NotTo(HaveOccurred())
			g.Expect(decoded).To(Equal(data))
		}
	})
}
