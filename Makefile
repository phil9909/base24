test:
	go test .

FUZZTIME ?= 1m
fuzz:
	go test . -fuzz=FuzzRoundTrip -fuzztime=$(FUZZTIME)
